<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>邮件测试</title>
    <asset:stylesheet href="layui/css/layui.css"/>
    <style>
        .main{width:80%;margin:5% auto;}
    </style>
</head>

<body>
    <div class="main">
        <h1>邮件测试</h1>
        <div>
            <button class="layui-btn layui-btn-normal" id="htmlMail">带HTML邮件</button>
            <button class="layui-btn layui-btn-warm" id="attachMail">带附件邮件</button>
        </div>
    </div>
    <asset:javascript src="layui/layui.js"/>

    <script>
        layui.use(["jquery","layer","form"],function () {
           var $ = layui.jquery,layer = layui.layer,form = layui.form;
           $("#htmlMail").click(function(){
                layer.open({
                    title: "带HTML",
                    type: 1,
                    area: ["400px","300px"],
                    btn: ["发送","取消"],
                    content: '<form class="layui-form layui-form-pane" action="" style="width:80%;margin:15px auto;">' +
                             ' <div class="layui-form-item">' +
                             '     <label class="layui-form-label">收件人邮箱</label>' +
                             '     <div class="layui-input-inline">' +
                             '         <input type="text" name="recipients" id="recipients" lay-verify="requeired|email" placeholder="请输入邮箱" autocomplete="off" class="layui-input">' +
                             '     </div>' +
                             ' </div>' +
                             ' <div class="layui-form-item">' +
                             '     <label class="layui-form-label">你的昵称</label>' +
                             '     <div class="layui-input-inline">' +
                             '         <input type="text" name="nickname" id="nickname" lay-verify="requeired|email" placeholder="请输入你的昵称" autocomplete="off" class="layui-input">' +
                             '     </div>' +
                             ' </div>' +
                             '</form>',
                    yes: function (index) {
                        var recipients = $("#recipients").val().trim();
                        var nickname = $("#nickname").val().trim();
                        if(recipients=='') {layer.msg("邮箱不能为空！");return false;}
                        if(nickname=='') {layer.msg("昵称不能为空！");return false;}
                        var load = layer.msg("邮件发送中...",{icon:16,time:100000,shade:["#000",0.5]});
                        $.post("${createLink(controller: "mail", action: "sendHtml")}",{recipients:recipients,nickname:nickname},function(res){
                            layer.close(load);
                            if(res.result==true) {
                                layer.alert(res.msg,{icon:1},function () {
                                    layer.closeAll();
                                });
                            }else{
                                layer.alert(res.msg,{icon:2});
                            }
                        });
                    }
                });
           });

            $("#attachMail").click(function(){
                layer.open({
                    title: "带附件",
                    type: 1,
                    area: ["400px","300px"],
                    btn: ["发送","取消"],
                    content: '<form class="layui-form layui-form-pane" action="" style="width:80%;margin:15px auto;">' +
                    ' <div class="layui-form-item">' +
                    '     <label class="layui-form-label">收件人邮箱</label>' +
                    '     <div class="layui-input-inline">' +
                    '         <input type="text" name="recipients" id="recipients" lay-verify="requeired|email" placeholder="请输入邮箱" autocomplete="off" class="layui-input">' +
                    '     </div>' +
                    ' </div>' +
                    ' <div class="layui-form-item">' +
                    '     <label class="layui-form-label">你的昵称</label>' +
                    '     <div class="layui-input-inline">' +
                    '         <input type="text" name="nickname" id="nickname" lay-verify="requeired|email" placeholder="请输入你的昵称" autocomplete="off" class="layui-input">' +
                    '     </div>' +
                    ' </div>' +
                    '</form>',
                    yes: function (index) {
                        var recipients = $("#recipients").val().trim();
                        var nickname = $("#nickname").val().trim();
                        if(recipients=='') {layer.msg("邮箱不能为空！");return false;}
                        if(nickname=='') {layer.msg("昵称不能为空！");return false;}
                        var load = layer.msg("邮件发送中...",{icon:16,time:100000,shade:["#000",0.5]});
                        $.post("${createLink(controller: "mail", action: "sendAttach")}",{recipients:recipients,nickname:nickname},function(res){
                            layer.close(load);
                            if(res.result==true) {
                                layer.alert(res.msg,{icon:1},function () {
                                    layer.closeAll();
                                });
                            }else{
                                layer.alert(res.msg,{icon:2});
                            }
                        });
                    }
                });
            });
        });
    </script>
</body>
</html>